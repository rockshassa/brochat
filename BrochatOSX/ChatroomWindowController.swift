//
//  ChatroomWindowController.swift
//  Brochat
//
//  Created by nick local on 2/21/15.
//
//

import Cocoa
import CoreLocation

class ChatroomWindowController: NSWindowController, NSTextFieldDelegate, NSTableViewDataSource, NSTableViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var titleLabel: NSTextField!
    @IBOutlet weak var tableView: NSTableView!
    @IBOutlet weak var textEntryField: NSTextField!
    @IBOutlet weak var usersTable: NSTableView!
    
    var tableCol:NSTableColumn?
    
    let locMgr = CLLocationManager()
    var lastLoc:CLLocation?
    
    let userName = "nick_osx"
    var messages:[String] = Array()
    var users:[User] = Array()
    
    let socket:SocketIOClient = SocketIOClient(socketURL: "54.186.39.168:3000", opts:[
        "reconnects": true, // default true
//        "reconnectAttempts": 5, // default -1 (infinite tries)
//        "reconnectWait": 5, // default 10
        ])
    
    override func windowDidLoad() {

        super.windowDidLoad()
        
        self.window?.title = "brochat osx client 1.0"

        titleLabel.stringValue = "brochat"
        
        self.textEntryField.delegate = self;
//        self.textEntryField.formatter
        
        self.tableView.setDataSource(self)
        self.tableView.setDelegate(self)
        self.tableView.rowHeight = 20
        self.tableView.columnAutoresizingStyle = NSTableViewColumnAutoresizingStyle.FirstColumnOnlyAutoresizingStyle
        
        self.usersTable.setDataSource(self)
        self.usersTable.setDelegate(self)
        self.usersTable.rowHeight = 20
        
        locMgr.delegate = self

        requestLocationPermissionIfNeeded()
        
        configureSocket()

    }
    
    func requestLocationPermissionIfNeeded(){
    
        println(__FUNCTION__)
        
        if (CLLocationManager.locationServicesEnabled()) {
            
            println("loc enabled")
            
            switch CLLocationManager.authorizationStatus() {
                
            case .Authorized:
                println("Authorized")
                self.locMgr.startUpdatingLocation()
                
            case .Denied:
                println("Denied")
                
            case .NotDetermined:
                println("Not Determined")
                self.locMgr.startUpdatingLocation()
                
            case .Restricted:
                println("Restricted")
                
            }
            
        } else {
            println("loc disabled")
        }
        
    }
    
    func configureSocket(){
        
        /*
        ok after you connect to the server and get a handle on the socket. you can now pass in an object with properties UserName, Lat, Lon to an initialize event. in javascript it'd be done like so:
        
        socket.emit('initialize',{ UserName : userName , Lat : lat , Lon : lon });
        
        After that the server should emit back a 'title' event with the room title, a 'usersInRoomUpdate' event with the list of members, a 'joined' event to broadcast the name of the member who joined, and finally an optional 'chatLoaded' event that you can handle to know the chat room is fully complete.
        
        receivedSocketEvents = ['message','title','joined','selfjoined','left','selfLeft','usersInRoomUpdate','userError',
        'messageHistory','injectMessage','selfMessage','imageMessage','selfImageMessage','userBooted',
        'lightMessage','selfLightMessage','chatLoaded'];
        */
        
        socket.on("connect") {data in
            
            println("socket connected")

            if let currentLoc = self.lastLoc {
                
                let lat = "\(currentLoc.coordinate.latitude)"
                let lon = "\(currentLoc.coordinate.longitude)"
                
                let dict = [ "UserName" : self.userName,
                                "Lat" : lat,
                                "Lon" : lon]
                
                self.socket.emit("initialize", dict)
                
            } else {
                
                println("no location available, did not connect")
            }
        }
        
        socket.on("chatLoaded"){ data in
             self.socket.emit("getMessageHistory")
        }
        
        socket.on("title") {data in
            
            if let roomName = data as? String {
                println("got room title: " + roomName)
                self.titleLabel.stringValue = roomName
            }
        }
        
        socket.on("usersInRoomUpdate") { data in
            
            if let users = data as? [Dictionary<String, String>]{
                
                self.users.removeAll(keepCapacity: true)
                
                for dict in users {
                    if let username = dict["Name"] {
                        println(username)
                        self.users.append(User(username: username))
                    }
                }
                
                self.usersTable.reloadData()
            }
        }
        
        socket.on("messageHistory"){ data in
         
            if let msgArray = data as? [AnyObject]{
                
                for dict in msgArray{
                    if let content:String = dict["Content"] as? String {
                        self.addMessage(content)
                    }
                }
            }
        }
        
        socket.on("message") { data in
            
            if let msg = data as? String {
                
                println(msg)
                self.addMessage(msg)
            }
        }
        
        socket.on("selfMessage") { data in
            
            if let msg = data as? String {
                
                println(msg)
                self.addMessage(msg)
            }
        }
        
        socket.on("weather") { data in
            
            if let weatherDict = data as? Dictionary<String, String> {
                
                var mut = ""
                
                if let tempStr = weatherDict["Temp"] {
                    mut = mut + tempStr
                }
                
                if let weatherStr = weatherDict["Weather"] {
                    mut = mut + " " + weatherStr
                }
                
                if (countElements(mut) > 0){
                    self.addMessage(mut)
                }
            }
        }
        
        socket.on("typing"){ data in
         
            if let un = data as? String {
                
                for user in self.users {
                 
                    if (user.un == un){
                        user.isTyping = true
                        self.usersTable.reloadData()
                    }
                    
                }
            }
        }
        
        socket.on("stopTyping"){ data in
            if let un = data as? String {
                
                for user in self.users {
                    
                    if (user.un == un){
                        user.isTyping = false
                        self.usersTable.reloadData()
                    }
                }
            }
        }
        
    }
    
    //stoppedTyping
    //notifyTyping
    
    func locationManager(manager: CLLocationManager!, didUpdateToLocation newLocation: CLLocation!, fromLocation oldLocation: CLLocation!) {
        println(__FUNCTION__)

        self.lastLoc = newLocation
        
        manager.stopUpdatingLocation()

        self.socket.open()
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println(__FUNCTION__)
        
        if let e = error {
            println(e.localizedDescription)
        }
    }
    
    @IBAction func returnPressed(sender: AnyObject) {
        
        let trimmedStr = self.textEntryField.stringValue.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        let strLen = countElements(trimmedStr)
        
        if  (strLen > 0){
            
            let fullMsg = userName + ":" + trimmedStr
            
            socket.emit("message", fullMsg)
            
            self.textEntryField.stringValue = ""
        }
    }
    
    func addMessage(msg:String){
        
        self.messages.append(msg)
        reloadTableAndScrollToBottom()
        
    }
    
    func reloadTableAndScrollToBottom(){
        println(self.messages)
        
        self.tableCol?.width = self.tableView.frame.size.width
        
        self.tableView.reloadData()
        
        self.tableView.scrollRowToVisible(self.tableView.numberOfRows-1)
    }
    
    func numberOfRowsInTableView(tableView: NSTableView) -> Int {
        
        if (tableView == self.tableView){
            
            return countElements(self.messages)
            
        } else if (tableView == self.usersTable){
            
            return countElements(self.users)
        }
        return 0
    }
    
    func tableView(tableView: NSTableView, viewForTableColumn tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        if (tableView == self.tableView){
            
            let reuseIdentifier = "someIdent"
//
//            var cell:NSTextField? = tableView.makeViewWithIdentifier(reuseIdentifier, owner: self) as? NSTextField
//            
//
//            cell?.stringValue = self.messages[row]
//            
//            return cell
            
            var cell:ChatCellController? = tableView.makeViewWithIdentifier(reuseIdentifier, owner: self) as? ChatCellController
            if (cell == nil) {
                cell = ChatCellController(nibName:"ChatCellController", bundle:nil)
                cell?.identifier = reuseIdentifier
            }
            
            return cell?.view
            
        } else if (tableView == self.usersTable){
            
            let reuseIdentifier = "someIdent2"
            
            var cell:NSTextField? = tableView.makeViewWithIdentifier(reuseIdentifier, owner: self) as? NSTextField
            
            if (cell == nil) {
                cell = NSTextField()
                cell?.lineBreakMode = NSLineBreakMode.ByWordWrapping
                cell?.identifier = reuseIdentifier
            }
            
            let user = self.users[row]
            var str = user.un
            
            if (user.isTyping){
                str = "*" + str
            }
            
            cell?.stringValue = str
            
            return cell
        }
        
        return nil
    }
}
