//
//  AppDelegate.swift
//  BrochatOSX
//
//  Created by nick local on 2/21/15.
//
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!

    var chatController:ChatroomWindowController?

    func applicationDidFinishLaunching(aNotification: NSNotification) {

        chatController = ChatroomWindowController(windowNibName: "ChatroomWindowController")

        chatController?.window?.makeKeyAndOrderFront(self)
        
//        self.windowController = [[[WhereIsMyMacWindowController alloc] init] autorelease];
//        [[self.windowController window] makeKeyAndOrderFront:self];
        
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

